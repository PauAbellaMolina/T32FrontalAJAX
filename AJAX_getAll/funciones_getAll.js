addEventListener('load',inicializarEventos, false);

function inicializarEventos() {
    var ob=document.getElementById('boton1');
    ob.addEventListener('click',presionBoton,false);
}

function presionBoton() {
    alert('Get all executed');
    getAll();
}

function getAll() {
    $.ajax({
        url: "http://localhost:8181/api/products",
        method: 'GET',
        dataType: 'json',
        header: {
            'Accept':'application/json',
            'Authorization':'Bearer '
        },
        contentType: 'application/x-www-form-urlencoded',

        success: function (data) {
            console.log(data.data[0].id);
            console.log(JSON.stringify(data));
        },
        error: function (error) {
            console.log(error);
        }
    });
}