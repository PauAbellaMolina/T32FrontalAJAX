addEventListener('load',inicializarEventos, false);

function inicializarEventos() {
    var ob=document.getElementById('boton1');
    ob.addEventListener('click',presionBoton,false);
}

function presionBoton() {
    postProduct('PT55','Producto de test');
}

function postProduct(nom, det) {
    $.ajax({
        url: "http://localhost:8181/api/products",
        method: 'POST',
        dataType: 'json',
        header: {
            'Accept':'application/json',
            'Authorization':'Bearer '
        },
        contentType: 'application/x-www-form-urlencoded',
        data: { name: nom, detail: det },
        success: function (data, textStatus, jqXHR) {
            alert('Product posted')
        },
        error: function (error) {
            console.log(error);
        }
    });
}